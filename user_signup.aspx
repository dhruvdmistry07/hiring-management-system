﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General_Master.Master" AutoEventWireup="true" CodeBehind="user_signup.aspx.cs" Inherits="IDP_Project_01.user_signup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>CANDIDATE SIGNUP</h2>
              <p></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li class="active">SIGN-UP</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
    <!-- Start contact section  -->
  <section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title">Candidate Signup</h2>
              <span class="line"></span>
              <p>Applicants can register here...</p>
            </div>
         </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
    <div class="col-md-8">
                 <div style="margin-left:470px;">
                   <form runat="server">
                    <div class="form-group">
    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
    <br />
    
    <asp:TextBox ID="TextBoxname" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxname" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    
    <br />
    <asp:Label ID="Label2" runat="server" Text="Contact"></asp:Label>
   
    <br />
    <asp:TextBox ID="TextBoxcon" runat="server" TextMode="Number"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxcon" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxcon" ErrorMessage="* Must be 10 numbers" ForeColor="Red" ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>
    
                <br />
    <br />
    <asp:Label ID="Label3" runat="server" Text="City"></asp:Label>
    
                <br />
    <asp:TextBox ID="TextBoxcity" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBoxcity" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
        <br />
    <asp:Label ID="Label4" runat="server" Text="Email"></asp:Label>
    <br />
    
    <asp:TextBox ID="TextBoxmail" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="TextBoxmail" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
    
                <br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxmail" ErrorMessage="* Must be mail format" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    
                <br />
    <br />
    <asp:Label ID="Label5" runat="server" Text="Passworrd"></asp:Label>
    
    <br />
    <asp:TextBox ID="TextBoxpwd" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxpwd" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
    
                <br />
    <br />
    <asp:Label ID="Label6" runat="server" Text="Retype Password"></asp:Label>
    
                <br />
    <asp:TextBox ID="TextBoxrpwd" runat="server" TextMode="Password"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxpwd" ControlToValidate="TextBoxrpwd" ErrorMessage="Password must be same as above" ForeColor="Red"></asp:CompareValidator>
    
                <br />
    <br />
    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Signup" />
    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Reset" />
    </div>
             
           
                <br />
    <br />
    <br />
    <br />
    <br />
    </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  <!-- End contact section  -->
    
  <!-- Start footer -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="footer-left">
           <p>Designed by <a href="http://www.markups.io/">MarkUps.io</a></p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="footer-right">
            <a href="index.html"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End footer -->

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="assets/js/waypoints.js"></script>
  <script src="assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="assets/js/bootstrap-progressbar.js"></script>  
  
 
  <!-- Custom js -->
  <script type="text/javascript" src="assets/js/custom.js"></script>
    

</asp:Content>


