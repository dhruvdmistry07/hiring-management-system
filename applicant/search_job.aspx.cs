﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.applicant
{
    public partial class search_job : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["appid"] == null)
                Response.Redirect("../user_login.aspx");
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
           
            string cid = DropDownList1.SelectedValue.ToString();
            string q = "select * from Jobtbl where category ='" + cid + "'";

            cmd = new SqlCommand(q, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Jobtbl");
            if (ds.Tables["Jobtbl"].Rows.Count > 0)
            {
                GridView1.DataSource = ds.Tables["Jobtbl"];
                GridView1.DataBind();

            }

        }
        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img;
            img = (ImageButton)sender;
            Response.Redirect("apply_job.aspx?jobid=" + img.CommandArgument.ToString());
        }
    }
}