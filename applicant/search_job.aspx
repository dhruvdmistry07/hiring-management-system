﻿<%@ Page Title="" Language="C#" MasterPageFile="~/applicant/UserMaster.Master" AutoEventWireup="true" CodeBehind="search_job.aspx.cs" Inherits="IDP_Project_01.applicant.search_job" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2>Search Job</h2><br />
    <asp:Label ID="lblcat" runat="server" Text="Select Category:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="CategoryName" DataValueField="CategoryName" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Cv_DBConnectionString %>" SelectCommand="SELECT * FROM [Categorytbl]"></asp:SqlDataSource>
    <br />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="1129px">
        <Columns>
            <asp:BoundField DataField="jobid" HeaderText="Job ID" />
            <asp:BoundField DataField="jobdate" HeaderText="Date of Posting" />
            <asp:BoundField DataField="jobtitle" HeaderText="Job Title" />
            <asp:BoundField DataField="job_type" HeaderText="Job Type" />
            <asp:BoundField DataField="location" HeaderText="Job Location" />
            <asp:TemplateField HeaderText="File">
                <ItemTemplate>
                    
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("jobfile", "{0}") %>' Target="_blank" ForeColor="#3366FF">Download</asp:HyperLink>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="salary" HeaderText="Salary" />
            <asp:BoundField DataField="salarytype" HeaderText="Salary Type" />
            <asp:TemplateField HeaderText="Apply">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/icons/apply.png" Width="85px" CommandArgument='<% #Eval("jobid", "{0}") %>' OnClick="ImageButton2_Click" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <br />
    <br />
</asp:Content>
