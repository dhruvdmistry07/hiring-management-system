﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IDP_Project_01.applicant
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["appid"] != null)
            {
                Session.Remove("appid");
                Response.Redirect("../user_login.aspx");

            }
            else
            {
                Response.Redirect("../user_login.aspx");

            }
        }
    }
}