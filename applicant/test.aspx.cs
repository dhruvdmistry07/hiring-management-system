﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.applicant
{
    public partial class test : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //int count = Convert.ToInt32(Session["count"].ToString());
                int count = 0;
                if (Session["appid"] == null)
                    Response.Redirect("../user_login.aspx");
                if (Session["Qid"] == null)
                {

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    // Gettting Application id 
                    string q1 = "select * from Application order by Applicationid desc";
                    cmd = new SqlCommand(q1, con);
                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    cmd.ExecuteNonQuery();
                    da.Fill(ds, "Application");
                    int appid = Convert.ToInt32(ds.Tables["Application"].Rows[0][0].ToString());
                    Session["Application_ID"] = appid.ToString();
                    string jid = Session["jobid"].ToString();
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    string q = "select * from Questiontbl where jobid ='" + Session["jobid"].ToString() + "'";
                    cmd = new SqlCommand(q, con);
                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    cmd.ExecuteNonQuery();
                    da.Fill(ds, "Questiontbl");

                    if (ds.Tables["Questiontbl"].Rows.Count > 0)
                    {
                        Session["Qid"] = ds.Tables["Questiontbl"].Rows[0][0].ToString();
                        Session["Qtitle"] = ds.Tables["Questiontbl"].Rows[0][2].ToString();
                        Session["TotalQuestions"] = ds.Tables["Questiontbl"].Rows[0][3].ToString();
                        Session["Row_number"] = 1;
                        lbltest.Text = Session["Qtitle"].ToString();

                    }
                    con.Close();
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    string q2 = "select * from Question_List_tbl where Qid = '" + Session["Qid"].ToString() + "'";
                    SqlCommand cmd2;
                    SqlDataAdapter da2;
                    DataSet ds2;

                    cmd2 = new SqlCommand(q2, con);
                    da2 = new SqlDataAdapter(cmd2);
                    ds2 = new DataSet();
                    cmd2.ExecuteNonQuery();
                    da2.Fill(ds2, "Question_List_tbl");
                    lblq.Text = ds2.Tables["Question_List_tbl"].Rows[0][0].ToString();
                    lblquestion.Text = ds2.Tables["Question_List_tbl"].Rows[0][1].ToString();
                    rdboption1.Text = ds2.Tables["Question_List_tbl"].Rows[0][2].ToString();
                    rdboption2.Text = ds2.Tables["Question_List_tbl"].Rows[0][3].ToString();
                    rdboption3.Text = ds2.Tables["Question_List_tbl"].Rows[0][4].ToString();
                    rdboption4.Text = ds2.Tables["Question_List_tbl"].Rows[0][5].ToString();
                    lblco.Text = ds2.Tables["Question_List_tbl"].Rows[0][6].ToString();

                    ViewState["score2"] = 0;
                    ViewState["correctans"] = ds2.Tables["Question_List_tbl"].Rows[0][6].ToString();
                    Session["total_rows"] = ds2.Tables["Question_List_tbl"].Rows.Count.ToString();
                }
            }
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string user_ans = "";
            int score1 = 0;
            if (Session["score"] == null)
                Session["score"] = 0;
            if (rdboption1.Checked == true)
                user_ans = "A";
            else if (rdboption2.Checked == true)
                user_ans = "B";
            else if (rdboption3.Checked == true)
                user_ans = "C";
            else
                user_ans = "D";
            string system_ans = lblco.Text;
            if (String.Compare(user_ans, system_ans) == 0)
            {
                score1 = Convert.ToInt32(Session["score"].ToString());
                score1 += 1;
                // view

                ViewState["score2"] = (Convert.ToInt32(ViewState["score2"].ToString()) + 1).ToString();
                Session["score"] = score1.ToString();
            }
            else
            {
                rdboption1.Checked = false;
                rdboption2.Checked = false;
                rdboption3.Checked = false;
                rdboption4.Checked = false;
            }

            // checking if all question are done than make update query
            int rowcount = Convert.ToInt32(Session["Row_number"].ToString());
            int total_rows = Convert.ToInt32(Session["total_rows"].ToString());

            if (rowcount >= total_rows)
            {
                // all questions displayed so exit from this
                // update the score for particular application
                string q1 = "update Application set Test_Score = '" + Session["score"].ToString() + "' where Applicationid = '" + Session["Application_ID"].ToString() + "'";
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                cmd = new SqlCommand(q1, con);
                int i = cmd.ExecuteNonQuery();
                Session.Remove("Row_number");
                Session.Remove("Qid");
                Session.Remove("Qtitle");
                Session.Remove("TotalQuestions");
                Session.Remove("Row_number");
                Session.Remove("Application_ID");

                // int my_score = Convert.ToInt32(Session["score"].ToString());
                //   Session.Remove("score");

                if (i > 0)
                {
                    Response.Write("<script> alert('Test Completed successfully.'); window.location = 'myjobapplication.aspx'; </script>");

                }
                else
                {
                    Response.Write("<script> alert('Test score error!!'); window.location = 'myjobapplication.aspx'; </script>");
                }


            }
            else
            {
                string q = "select * from Question_List_tbl where Qid ='" + Session["Qid"].ToString() + "'";

                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                cmd.ExecuteNonQuery();
                da.Fill(ds, "Question_List_tbl");
                // int total_rows = 0;
                //int rowcount = 0;
                if (ds.Tables["Question_List_tbl"].Rows.Count > 0)
                {

                    total_rows = Convert.ToInt32(ds.Tables["Question_List_tbl"].Rows.Count.ToString());
                    Session["total_rows"] = total_rows;

                    //rowcount = Convert.ToInt32(Session["Row_number"].ToString());




                    rowcount = Convert.ToInt32(Session["Row_number"].ToString());
                    total_rows = Convert.ToInt32(Session["total_rows"].ToString());

                    if (rowcount < total_rows)
                    {



                        lblq.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][0].ToString();
                        lblquestion.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][1].ToString();
                        rdboption1.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][2].ToString();
                        rdboption2.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][3].ToString();
                        rdboption3.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][4].ToString();
                        rdboption4.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][5].ToString();
                        lblco.Text = ds.Tables["Question_List_tbl"].Rows[rowcount][6].ToString();
                        ViewState["correctans"] = ds.Tables["Question_List_tbl"].Rows[rowcount][6].ToString();

                        rowcount += 1;
                        Session["Row_number"] = rowcount.ToString();
                    }
                }




            }
        }
    }
}