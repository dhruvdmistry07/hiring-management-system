﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.applicant
{
    public partial class profile : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["appid"] == null)
                    Response.Redirect("../user_login.aspx");
                string appid = Session["appid"].ToString();
                string q = "select * from Applicant where appid='" + appid + "'";
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                cmd.ExecuteNonQuery();
                da.Fill(ds, "Applicant");
                txtname.Text = ds.Tables["Applicant"].Rows[0][1].ToString();
                txtemail.Text = ds.Tables["Applicant"].Rows[0][3].ToString();
                txtcnumber.Text = ds.Tables["Applicant"].Rows[0][2].ToString();


            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string aid = Session["appid"].ToString();
            string q = "update Applicant set appname='" + txtname.Text + "',appcontact='"+ txtcnumber.Text +"',appemail='" + txtemail.Text + "' where appid='" + aid + "'";

            if (con.State == ConnectionState.Closed)
                con.Open();
            cmd = new SqlCommand(q, con);


            int i = cmd.ExecuteNonQuery();
            if (i > 0)
            {
                Response.Write("<script>alert('Profile updated sucessfully!'); window.location = 'Dashboard.aspx';</script>");
            }
            else
            {
                Response.Write("<script>alert ('Profile updated failed!'); window.location = 'profile.aspx';</script>");
            }
        }
    }
}