﻿<%@ Page Title="" Language="C#" MasterPageFile="~/applicant/UserMaster.Master" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="IDP_Project_01.applicant.profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2>Modify Profile</h2><br />
    <asp:Label ID="lblname" runat="server" Text="Name:"></asp:Label>
    <br />
    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtname" ErrorMessage="  *Name is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblcontact" runat="server" Text="Contact number:"></asp:Label>
    <br />
    <asp:TextBox ID="txtcnumber" runat="server" TextMode="Number"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtcnumber" ErrorMessage="  *Contact Number is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcnumber" ErrorMessage="*Invalid contact number!" ForeColor="Red" ValidationExpression="^[6-9]\d{9}$"></asp:RegularExpressionValidator>
    <br />
    <br />
    <asp:Label ID="lblemailid" runat="server" Text="Email ID:"></asp:Label>
    <br />
    <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtemail" ErrorMessage="  *Email is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtemail" ErrorMessage="*Email ID invalid!" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    <br />
    <br />
    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Update" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" CausesValidation="False" Text="Reset" />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
