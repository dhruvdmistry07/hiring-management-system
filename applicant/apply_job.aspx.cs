﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.applicant
{
    public partial class apply_job : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {


                if (Session["appid"] == null)
                    Response.Redirect("../user_login.aspx");

                if (Request.QueryString["jobid"] != null)
                {
                    ViewState["jobid"] = Request.QueryString["jobid"].ToString();
                    Session["jobid"] = Request.QueryString["jobid"].ToString();

                    Session["count"] = 0;
                    txtname.Text = Session["appname"].ToString();
                    //txteid.Text = Session["appemail"].ToString();
                    //txtcnumber.Text = Session["appcontact"].ToString();

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();

                    }
                    string q1 = "select * from Jobtbl where jobid ='" + Session["jobid"].ToString() + "'";
                    SqlCommand cmd2;
                    SqlDataAdapter da2;
                    DataSet ds3;
                    cmd2 = new SqlCommand(q1, con);
                    da2 = new SqlDataAdapter(cmd2);
                    ds3 = new DataSet();

                    cmd2.ExecuteNonQuery();
                    da2.Fill(ds3, "Jobtbl");
                    if (ds3.Tables["Jobtbl"].Rows.Count > 0)
                    {
                        ViewState["job_skills"] = ds3.Tables["Jobtbl"].Rows[0][5].ToString();
                        ViewState["job_exp"] = ds3.Tables["Jobtbl"].Rows [0][11].ToString();
                        con.Close();

                    }
                }
                
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            //string qdesc = "select [jobdesc] from Jobtbl where jobid=" + ViewState["jobid"].ToString() + "'";
            //string exp = "select [experience] from Jobtbl where jobid=" + ViewState["jobid"].ToString() + "'";
            string filenm = "", filepath, fileext;
            filepath = "../uploaduser/";
            if (FileUpload1.HasFile)
            {
                fileext = Path.GetExtension(FileUpload1.FileName); //gets the extension of the uploaded file name
                if ((fileext == ".pdf") || (fileext == ".doc") || (fileext == ".docx"))
                {
                    filenm = FileUpload1.FileName; //gets name of the uploaded file
                    FileUpload1.SaveAs(Server.MapPath(filepath) + filenm); //severmap gets the file path from the parent system
                    lblstatus.Text = "File uploaded successfully" + Server.MapPath(filepath);
                    int skills_score = 0;
                    string user_skills = txtskills.Text;
                    string[] skills = user_skills.Split(',');
                    for (int k = 0; k < skills.Length; k++)
                    {
                        skills[k] = skills[k].ToUpper();

                    }
                    string required_skills = ViewState["job_skills"].ToString();
                    string[] job_req_skills = required_skills.Split(',');
                    for (int l = 0; l < job_req_skills.Length; l++)
                    {
                        job_req_skills[l] = job_req_skills[l].ToUpper();
                    }

                    foreach (string require_job_skill in job_req_skills)
                    {
                        foreach (string usr_skill in skills)
                        {
                            if (usr_skill.CompareTo(require_job_skill) == 0)//usr_skill.CompareTo(require_job_skill) == 0
                            {
                                skills_score += 5;
                            }
                        }
                    }


                    filepath = "../uploaduser/" + filenm;
                    string q = "insert into Application(jobid,appid,details,CV_File,Test_Score,Application_Status,Name,address,city,pincode,email,skills_score,education,experience)" +
                        " values ('" + ViewState["jobid"].ToString() + "','" + Session["appid"].ToString() + "','" + txtskills.Text + "','"+filepath + "','" + '0' + "','" + "PENDING" + "','" + txtname.Text + "','" + txtadd.Text + "','" + txtcty.Text + "','" + txtpin.Text + "','" + txteid.Text + "','" + skills_score + "','" + txtedu.Text + "','" + txtexp.Text + "')";
                    cmd = new SqlCommand(q, con);
                    int i = cmd.ExecuteNonQuery();
                    int minexp = Convert.ToInt32(ViewState["job_exp"].ToString());
                    
                    int appexp = Convert.ToInt32(txtexp.Text);
                   
                    if (i > 0)
                    {
                        if (minexp < appexp)
                        {
                            Response.Write("<script>alert('Application completed sucessfully! No test required!'); window.location = 'Dashboard.aspx';</script>");
                        }
                        else
                        {
                            Response.Write("<script>alert('Application completed sucessfully! Redirecting to test!'); window.location = 'test.aspx';</script>");
                        }                     
                    }
                    else
                    {
                        Response.Write("<script>alert('Error! Try again.'); window.location = 'apply_job.aspx';</script>");
                    }

                }
                else
                {
                    lblstatus.Text = "Select only PDF, DOCX or DOC type";
                }
            }

            else
            {
                lblstatus.Text = "Please Select your file";
            }

        }
    }
}
