﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


namespace IDP_Project_01.applicant
{
    public partial class changepass : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["appid"] == null)
                    Response.Redirect("../user_login.aspx");
            }

        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            string appid = Session["appid"].ToString();

            string q = "select * from Applicant where appid = '" + appid + "'";
            cmd = new SqlCommand(q, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            cmd.ExecuteNonQuery();
            da.Fill(ds, "Applicant");
            string pwd = ds.Tables["Applicant"].Rows[0][4].ToString();
            string cp = txtcp.Text;
            if (cp == pwd)
            {
                string q1 = "update Applicant set apppwd = '" + txtrp.Text + "' where appid ='" + appid + "'";
                cmd = new SqlCommand(q1, con);
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    Response.Write("<script>alert('Password Updated successfully!');window.location='Dashboard.aspx';</script>");
                }
                else
                {
                    Response.Write("<script>alert('Password update Failed!');window.location='changepass.aspx';</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Wrong current password!');window.location='Reset_pass.aspx';</script>");
            }
        }
    }
}