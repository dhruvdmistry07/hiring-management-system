﻿<%@ Page Title="" Language="C#" MasterPageFile="~/applicant/UserMaster.Master" AutoEventWireup="true" CodeBehind="apply_job.aspx.cs" Inherits="IDP_Project_01.applicant.apply_job" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2>Apply For Job</h2><br />
    <asp:Label ID="lblname" runat="server" Text="Name:"></asp:Label>
    <br />
    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblcontact" runat="server" Text="Contact Number:"></asp:Label>
    <br />
    <asp:TextBox ID="txtcnumber" runat="server" TextMode="Number"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lbladdrss" runat="server" Text="Address:"></asp:Label>
    <br />
    <asp:TextBox ID="txtadd" runat="server" TextMode="MultiLine"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblcity" runat="server" Text="City:"></asp:Label>
    <br />
    <asp:TextBox ID="txtcty" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblpin" runat="server" Text="Pincode:"></asp:Label>
    <br />
    <asp:TextBox ID="txtpin" runat="server" TextMode="Number"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lbleid" runat="server" Text="Email ID:"></asp:Label>
    <br />
    <asp:TextBox ID="txteid" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblucv" runat="server" Text="Upload CV:"></asp:Label>
    <br />
    <asp:FileUpload ID="FileUpload1" runat="server" />
    <br />
    <asp:Label ID="lblstatus" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblskills" runat="server" Text="Your Skills: (Seperated by commas)"></asp:Label>
    <br />
    <asp:TextBox ID="txtskills" runat="server" TextMode="MultiLine"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lbledu" runat="server" Text="Education:"></asp:Label>
    <br />
    <asp:TextBox ID="txtedu" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblexp" runat="server" Text="Experience:"></asp:Label>
    <br />
    <asp:TextBox ID="txtexp" runat="server" TextMode="Number"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" Text="Reset" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
