﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.applicant
{
    public partial class myjobapplication : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["appid"] == null)
                    Response.Redirect("../user_login.aspx");
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }

                string q = "select * from Application where appid = '" + Session["appid"].ToString() + "'";
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                cmd.ExecuteNonQuery();

                da.Fill(ds, "Application");
                if (ds.Tables["Application"].Rows.Count > 0)
                {

                    GridView1.DataSource = ds.Tables["Application"];
                    GridView1.DataBind();


                }
            }
        }
    }
}