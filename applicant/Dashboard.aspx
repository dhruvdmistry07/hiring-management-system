﻿<%@ Page Title="" Language="C#" MasterPageFile="~/applicant/UserMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="IDP_Project_01.applicant.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
         <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="dbox dbox--color-1">
                                <div class="dbox__icon">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">8,252</span>
                                    <span class="dbox__title">Total Jobs</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="search_job.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dbox dbox--color-2">
                                <div class="dbox__icon">
                                    <i class="fa fa fa-edit"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">100</span>
                                    <span class="dbox__title">Total Applications</span>
                                </div>
                                
                                             
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dbox dbox--color-3">
                                <div class="dbox__icon">
                                    <i class="fa fa-question-circle"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">157</span>
                                    <span class="dbox__title">Total Tests</span>
                                </div>
                                
                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  
</asp:Content>
