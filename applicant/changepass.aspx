﻿<%@ Page Title="" Language="C#" MasterPageFile="~/applicant/UserMaster.Master" AutoEventWireup="true" CodeBehind="changepass.aspx.cs" Inherits="IDP_Project_01.applicant.changepass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2>Change Password</h2>
    <br />
    <asp:Label ID="lblcp" runat="server" Text="Current Passeword:"></asp:Label>
    <br />
    <asp:TextBox ID="txtcp" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcp" ErrorMessage="  *Current Password is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblnp" runat="server" Text="New Password:"></asp:Label>
    <br />
    <asp:TextBox ID="txtnp" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtnp" ErrorMessage="  *New Password is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblrp" runat="server" Text="Re-type Password:"></asp:Label>
    <br />
    <asp:TextBox ID="txtrp" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtrp" ErrorMessage="  *This field is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtnp" ControlToValidate="txtrp" ErrorMessage="*Password Mismatch!" ForeColor="Red"></asp:CompareValidator>
    <br />
    <br />
    <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" Text="Update" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" Text="Reset" />
    <br />
    <br />
    <br />
</asp:Content>
