Console.WriteLine("Comma separated strings");  
// String of authors  
string authors = "Mahesh Chand, Henry He, Chris Love, Raj Beniwal, Praveen Kumar";  
// Split authors separated by a comma followed by space  
string[] authorsList = authors.Split(", ");  
foreach (string author in authorsList)  
Console.WriteLine(author);