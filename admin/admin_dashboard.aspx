﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminmaster.Master" AutoEventWireup="true" CodeBehind="admin_dashboard.aspx.cs" Inherits="IDP_Project_01.admin.admin_dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
         <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="dbox dbox--color-1">
                                <div class="dbox__icon">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">8,252</span>
                                    <span class="dbox__title">Manage Users</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="ManageApplicants.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dbox dbox--color-2">
                                <div class="dbox__icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">100</span>
                                    <span class="dbox__title">Manage HR</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="ManageHr.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dbox dbox--color-3">
                                <div class="dbox__icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">2502</span>
                                    <span class="dbox__title">Manage Jobs</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="Managejob.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>
