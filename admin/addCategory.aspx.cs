﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


namespace IDP_Project_01.admin
{
    public partial class addCategory : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["aid"] == null)
                Response.Redirect("../admin_login.aspx");

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            string q = "insert into Categorytbl(Categoryname)" +
                            " values ('" + txtaddcat.Text + "')";
            cmd = new SqlCommand(q, con);
            int i = cmd.ExecuteNonQuery();
            if (i > 0)
            {
                Response.Write("<script>alert('Category uploaded sucessfully!'); window.location = 'Managecategory.aspx';</script>");
            }
            else
            {
                Response.Write("<script>alert('Category upload failed!'); window.location = 'addHR.aspx';</script>");
            }


        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            txtaddcat.Text = null;
        }
    }
}