﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/adminmaster.Master" AutoEventWireup="true" CodeBehind="addCategory.aspx.cs" Inherits="IDP_Project_01.admin.addCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
    </p>
    <h2>Add Category</h2><br />
    <br />
    <asp:Label ID="lbladd" runat="server" Text="Add category:"></asp:Label>
    <br />
    <asp:TextBox ID="txtaddcat" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtaddcat" ErrorMessage="  *Category name is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" Text="Reset" CausesValidation="False" OnClick="btnreset_Click" />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
