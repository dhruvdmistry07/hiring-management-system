﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="reset_pass.aspx.cs" Inherits="IDP_Project_01.hr.reset_pass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Reset Password</h2>
    <br />
    <asp:Label ID="lblcp" runat="server" Text="Current Password:"></asp:Label>
    <br />
    <asp:TextBox ID="txtcpass" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcpass" ErrorMessage="  * Current password is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblnp" runat="server" Text="New Password:"></asp:Label>
    <br />
    <asp:TextBox ID="txtnpass" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtnpass" ErrorMessage="  * New password is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblrrp" runat="server" Text="Re-type Password:"></asp:Label>
    <br />
    <asp:TextBox ID="txtrpass" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtrpass" ErrorMessage="  * Re-typing new password is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtnpass" ControlToValidate="txtrpass" ErrorMessage="* Password mismatch!" ForeColor="Red"></asp:CompareValidator>
    <br />
    <br />
    <asp:Button ID="btsupdate" runat="server" OnClick="btsupdate_Click" Text="Update" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" Text="Reset" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
