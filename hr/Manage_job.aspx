﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="Manage_job.aspx.cs" Inherits="IDP_Project_01.hr.Manage_job" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="jobid" HeaderText="ID" />
            <asp:BoundField DataField="jobdate" HeaderText="Date of Posting" />
            <asp:BoundField DataField="jobtitle" HeaderText="Job Title" />
            <asp:BoundField DataField="category" HeaderText="Job Category" />
            <asp:BoundField DataField="job_type" HeaderText="Job Type" />
            <asp:BoundField DataField="jobdesc" HeaderText="Job Description" />
            <asp:BoundField DataField="jobfile" HeaderText="Job File" />
            <asp:BoundField DataField="location" HeaderText="Job Location" />
            <asp:BoundField DataField="pincode" HeaderText="Pincode" />
            <asp:BoundField DataField="salary" HeaderText="Salary" />
            <asp:BoundField DataField="salarytype" HeaderText="Salary Type" />
            <asp:TemplateField HeaderText="Test Details">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/icons/update.png" Width="70px" CommandArgument='<% #Eval("jobid", "{0}") %>' OnClick="ImageButton2_Click"  />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Remove">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/icons/delete.png" Width="40px" CommandArgument='<% #Eval("jobid", "{0}") %>' OnClick="ImageButton1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
