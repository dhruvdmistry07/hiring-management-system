﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class SelectCandidates : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");

                string q = "select * from Application where Application_Status ='PENDING'";
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                cmd.ExecuteNonQuery();
                da.Fill(ds, "Application");
                if (ds.Tables["Application"].Rows.Count > 0)
                {
                    GridView1.DataSource = ds.Tables["Application"];
                    GridView1.DataBind();

                }


            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();

            }

            string q = "select * from Application where jobid = '" + DropDownList2.SelectedValue.ToString() + "' and Application_Status='PENDING' order by skills_score desc, Test_Score desc";
            cmd = new SqlCommand(q, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            cmd.ExecuteNonQuery();

            da.Fill(ds, "Application");
            if (ds.Tables["Application"].Rows.Count > 0)
            {
                GridView1.DataSource = ds.Tables["Application"];
                GridView1.DataBind();
                // chart1.DataSource = ds.Tables["Application"];

            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            // accepted

            ImageButton imgbtn;
            imgbtn = (ImageButton)sender;
            string q = "update Application set Application_Status = 'ACCEPTED' where Applicationid ='" + imgbtn.CommandArgument.ToString() + "'";
            if (con.State == ConnectionState.Closed)
            {
                con.Open();

            }
            cmd = new SqlCommand(q, con);

            int i = cmd.ExecuteNonQuery();

            if (i > 0)
                Response.Redirect(Request.RawUrl);

        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            //reject


            ImageButton imgbtn;
            imgbtn = (ImageButton)sender;
            string q = "update Application set Application_Status = 'REJECT' where Applicationid ='" + imgbtn.CommandArgument.ToString() + "'";
            if (con.State == ConnectionState.Closed)
            {
                con.Open();

            }
            cmd = new SqlCommand(q, con);

            int i = cmd.ExecuteNonQuery();

            if (i > 0)
            
            {
                Response.Redirect(Request.RawUrl);
                // GridView1.DataBind();

            }
        }
    }
}