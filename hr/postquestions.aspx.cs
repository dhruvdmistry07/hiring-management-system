﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class postquestions : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                
                
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");
                
                if (Request.QueryString["Qid"] != null)
                {
                    ViewState["Qid"] = Request.QueryString["Qid"].ToString();

                }
                if (Request.QueryString["jobid"] != null)
                {
                    ViewState["jobid"] = Request.QueryString["jobid"].ToString();

                }
            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            string q = "insert into Question_List_tbl(Qid,Question,A_option,B_option,C_option,D_option,CorrectAns)" +
                            " values ('" + ViewState["Qid"].ToString() + "','" + txtquest.Text + "','" + txta.Text + "','"+ txtb.Text + "','" + txtc.Text + "','" +txtd.Text+ "','" + DropDownList2.SelectedValue +"')";
            cmd = new SqlCommand(q, con);
            int i = cmd.ExecuteNonQuery();
            if (i > 0)
            {
                Response.Write("<script>alert('Question uploded sucessfully!'); window.location = 'Manage_job.aspx';</script>");
            }
            else
            {
                Response.Write("<script>alert('Question uploded failed!'); window.location = 'postquestion.aspx';</script>");
            }
            //ViewState["jobid"] = Request.QueryString["jobid"].ToString();

        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            txta.Text = txtb.Text = txtc.Text = txtd.Text = txtquest.Text = DropDownList2.SelectedValue = null;
        }
    }
}