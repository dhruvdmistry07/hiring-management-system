﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IDP_Project_01.hr
{
    public partial class logouthr : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["hrid"] != null)
            {
                Session.Remove("hrid");
                Response.Redirect("../hr_login.aspx");

            }
            else
            {
                Response.Redirect("../hr_login.aspx");

            }
        }
    }
}