﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class postjob : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");
            }
            
        }

        protected void btnsub_Click(object sender, EventArgs e)
        {
            
                if (con.State == ConnectionState.Closed)
                    con.Open();
                //string q1 = "select jobid from Jobtbl "
                string filenm= "", filepath, fileext;
                filepath = "../uploadhr/";
                Random random = new Random();
                string val = random.Next(999,10000).ToString();

                if (FileUpload1.HasFile)
                {
                    fileext = Path.GetExtension(FileUpload1.FileName); //gets the extension of the uploaded file name
                    if ((fileext == ".pdf") || (fileext == ".doc") || (fileext == ".docx"))
                    {   
                        filenm = FileUpload1.FileName; //gets name of the uploaded file
                        
                        FileUpload1.SaveAs(Server.MapPath(filepath) + val + "Job_details" + fileext); //severmap gets the file path from the parent system
                        lblstatus.Text = "File uploaded successfully" + Server.MapPath(filepath);
                        filenm = "../uploadhr/" + val + "Job_details" + fileext;

                        string s = "";
                        if (RadioButton1.Checked == true)
                        {
                            s = "Per Month";
                        }
                        else
                        {
                            s = "Per Annum";
                        }
                        filepath = "../uploadhr/" + filenm;
                        string q = "insert into Jobtbl(jobtitle,category,job_type,jobdesc,jobfile,location,pincode,salary,salarytype,experience)" +
                            " values ('" + txttitle.Text + "','" + DropDownList2.SelectedValue + "','" + DropDownList1.SelectedValue + "','" + txtdesc.Text + "','" + filenm + "','" + txtlocation.Text + "','" + txtpin.Text + "','" + txtsal.Text + "','" + s + "','" + txtexp.Text +"')";
                        cmd = new SqlCommand(q, con);
                        int i = cmd.ExecuteNonQuery();
                        if (i > 0)
                        {
                            Response.Write("<script>alert('Job uploded sucessfully!'); window.location = 'postjob.aspx';</script>");
                            //Response.Write();
                        }
                        else
                        {
                            Response.Write("<script>alert('Job uploded failed!'); window.location = 'postjob.aspx';</script>");
                        }




                    }
                    else
                    {
                        lblstatus.Text = "Select only PDF, DOCX or DOC type";
                    }
                }

                else
                {
                    lblstatus.Text = "Please Select your file";
                }

                

            
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            txttitle.Text = txtsal.Text = txtpin.Text = txtlocation.Text = txtdesc.Text = DropDownList1.SelectedValue = DropDownList2.SelectedValue = null;
        }
    }
}