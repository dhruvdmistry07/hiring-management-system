﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class Manage_job : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");

                string q = "select * from Jobtbl";
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                cmd.ExecuteNonQuery();
                da.Fill(ds, "Jobtbl");
                if (ds.Tables["Jobtbl"].Rows.Count > 0)
                {
                    GridView1.DataSource = ds.Tables["Jobtbl"];
                    GridView1.DataBind();

                }


            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img;
            img = (ImageButton)sender;
            string q = "delete from Jobtbl where jobid = '" + img.CommandArgument.ToString() + "'";
            if (con.State == ConnectionState.Closed)
                con.Open();
            cmd = new SqlCommand(q, con);
            int i = cmd.ExecuteNonQuery();
            if (i > 0)
                Response.Redirect(Request.RawUrl);
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img;
            img = (ImageButton)sender;
            Response.Redirect("manage_question.aspx?jobid='" + img.CommandArgument.ToString() + "'");

        }
    }
}