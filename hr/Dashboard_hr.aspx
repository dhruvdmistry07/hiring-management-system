﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="Dashboard_hr.aspx.cs" Inherits="IDP_Project_01.hr.Dashboard_hr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="dbox dbox--color-1">
                                <div class="dbox__icon">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">8,252</span>
                                    <span class="dbox__title">Manage Jobs</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="Manage_job.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dbox dbox--color-2">
                                <div class="dbox__icon">
                                    <i class="fa fa fa-edit"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">100</span>
                                    <span class="dbox__title">Manage Test</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="manage_question.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dbox dbox--color-3">
                                <div class="dbox__icon">
                                    <i class="fa fa-question-circle"></i>
                                </div>
                                <div class="dbox__body">
                                    <span class="dbox__count">2502</span>
                                    <span class="dbox__title">Manage Questions</span>
                                </div>
                                
                                <div class="dbox__action">
                                    <a href="ManageQuestions.aspx" class="dbox__action__btn">More Info</a>
                                </div>              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
</asp:Content>
