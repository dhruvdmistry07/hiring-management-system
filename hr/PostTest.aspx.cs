﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class PostTest : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");
            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            string q = "insert into Questiontbl(jobid,Qtitle,TotalQuestions)" +
                            " values ('" + DropDownList1.SelectedValue + "','" + txttitle.Text + "','" + txttot.Text + "')";
            cmd = new SqlCommand(q, con);
            int i = cmd.ExecuteNonQuery();
            if (i > 0)
            {
                Response.Write("<script>alert('Test uploded sucessfully!'); window.location = 'manage_question.aspx';</script>");
            }
            else
            {
                Response.Write("<script>alert('Test uploded failed!'); window.location = 'PostTest.aspx';</script>");
            }

        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            txttitle.Text = txttot.Text = DropDownList1.SelectedValue = null;
        }
    }
}