﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="manage_question.aspx.cs" Inherits="IDP_Project_01.hr.manage_question" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Manage Tests</h2>
    <asp:Button ID="btnadd" runat="server" OnClick="btnadd_Click" Text="Add Test" />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="Qid" HeaderText="ID" />
            <asp:BoundField DataField="jobid" HeaderText="Job ID" />
            <asp:BoundField DataField="Qtitle" HeaderText="Test Title" />
            <asp:BoundField DataField="TotalQuestions" HeaderText="Total Questions" />
            <asp:TemplateField HeaderText="View Details">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/icons/update.png" Width="70px" CommandArgument='<% #Eval("Qid", "{0}") %>' OnClick="ImageButton2_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Remove">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/icons/delete.png" Width="40px" CommandArgument='<% #Eval("Qid", "{0}") %>' OnClick="ImageButton1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Add Questions">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/icons/add.png" Width="85px" CommandArgument='<% #Eval("Qid", "{0}") %>' OnClick="ImageButton3_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
