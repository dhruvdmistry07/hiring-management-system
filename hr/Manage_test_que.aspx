﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="Manage_test_que.aspx.cs" Inherits="IDP_Project_01.hr.Manage_test_que" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Manage Questions</h2>
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="QDid" HeaderText="ID" />
            <asp:BoundField DataField="Question" HeaderText="Question" />
            <asp:BoundField DataField="A_option" HeaderText="Option A" />
            <asp:BoundField DataField="B_option" HeaderText="Option B" />
            <asp:BoundField DataField="C_option" HeaderText="Option C" />
            <asp:BoundField DataField="D_option" HeaderText="Option D" />
            <asp:BoundField DataField="CorrectAns" HeaderText="Correct Answer" />
            <asp:BoundField DataField="Qid" HeaderText="Test ID" />
            <asp:TemplateField HeaderText="Remove">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/icons/delete.png" Width="40px" CommandArgument='<% #Eval("QDid", "{0}") %>' OnClick="ImageButton1_Click" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
