﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="postjob.aspx.cs" Inherits="IDP_Project_01.hr.postjob" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Post Job</h2>
    <asp:Label ID="lbltitle" runat="server" Text="Job title:"></asp:Label>
    <br />
    <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttitle" ErrorMessage="  *Job title is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblcat" runat="server" Text="Job Category:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource1" DataTextField="CategoryName" DataValueField="CategoryName">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Cv_DBConnectionString %>" SelectCommand="SELECT * FROM [Categorytbl]"></asp:SqlDataSource>
    <br />
    <br />
    <br />
    <asp:Label ID="lbltype" runat="server" Text="Job Type:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server">
        <asp:ListItem Selected="True">Full-Time</asp:ListItem>
        <asp:ListItem>Part-Time</asp:ListItem>
        <asp:ListItem>Internship</asp:ListItem>
    </asp:DropDownList>
    <br />
    <br />
    <br />
    <asp:Label ID="lbldesc" runat="server" Text="Job description:"></asp:Label>
    <br />
    <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtdesc" ErrorMessage="  *Job description is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblloc" runat="server" Text="Job location:"></asp:Label>
    <br />
    <asp:TextBox ID="txtlocation" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtlocation" ErrorMessage="  *Job location is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblpin" runat="server" Text="Pincode:"></asp:Label>
    <br />
    <asp:TextBox ID="txtpin" runat="server" TextMode="Number"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtpin" ErrorMessage="  *Pincode is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtpin" ErrorMessage="  *Invalid pincode format!" ForeColor="Red" ValidationExpression="^[1-9][0-9]{5}$"></asp:RegularExpressionValidator>
    <br />
    <br />
    <asp:Label ID="lblsal" runat="server" Text="Salary:"></asp:Label>
    <br />
    <asp:TextBox ID="txtsal" runat="server" TextMode="Number"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtsal" ErrorMessage="  *Salary is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblsaltype" runat="server" Text="Salary type:"></asp:Label>
    <br />
    <asp:RadioButton ID="RadioButton1" runat="server" Checked="True" Text=" Per Month" ValidationGroup="saltype" GroupName="saltype" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:RadioButton ID="RadioButton2" runat="server" Text=" Per Annum" ValidationGroup="saltype" GroupName="saltype" />
&nbsp;&nbsp;&nbsp;
    <br />
    <br />
    <br />
    <asp:Label ID="lblfile" runat="server" Text="Job file:"></asp:Label>
    <br />
    <asp:FileUpload ID="FileUpload1" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="FileUpload1" ErrorMessage="  *Job file is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="lblstatus" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblexp" runat="server" Text="Experience Required:"></asp:Label>
    <br />
    <asp:TextBox ID="txtexp" runat="server" TextMode="Number"></asp:TextBox>
    <br />
    <br />
    <br />
    <asp:Button ID="btnsub" runat="server" OnClick="btnsub_Click" Text="Submit" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" Text="Reset:" OnClick="btnreset_Click" />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
