﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class reset_pass : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");
            }

        }

        protected void btsupdate_Click(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            string hrid = "2";

            string q = "select * from hrtbl where hrid = '" + hrid + "'";
            cmd = new SqlCommand(q, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            cmd.ExecuteNonQuery();
            da.Fill(ds, "hrtbl");
            string pwd = ds.Tables["hrtbl"].Rows[0][4].ToString();
            string cp = txtcpass.Text;
            if (cp == pwd)
            {
                string q1 = "update hrtbl set hrpwd = '" + txtrpass.Text + "' where hrid ='" + hrid + "'";
                cmd = new SqlCommand(q1, con);
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    Response.Write("<script>alert('Password Updated successfully!');window.location='Dashboard_hr.aspx';</script>");
                }
                else
                {
                    Response.Write("<script>alert('Password update Failed!');window.location='reset_pass.aspx';</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Wrong current password!');window.location='reset_pass.aspx';</script>");
            }

        }
    }
}