﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="SelectCandidates.aspx.cs" Inherits="IDP_Project_01.hr.SelectCandidates" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
    </p>
    <br />
    <asp:Label ID="lblcat" runat="server" Text="Select Category:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="CategoryName" DataValueField="Categoryid">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Cv_DBConnectionString %>" SelectCommand="SELECT * FROM [Categorytbl]"></asp:SqlDataSource>
    <br />
    <br />
    <asp:Label ID="lbljob" runat="server" Text="Select Job:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource2" DataTextField="jobtitle" DataValueField="jobid" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Cv_DBConnectionString %>" SelectCommand="SELECT * FROM [Jobtbl]"></asp:SqlDataSource>
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="Applicationid" HeaderText="Application ID" />
            <asp:BoundField DataField="ApplicationDt" HeaderText="Application Date" />
            <asp:BoundField DataField="jobid" HeaderText="Job ID" />
            <asp:BoundField DataField="appid" HeaderText="Applicant ID" />
            <asp:BoundField DataField="Test_Score" HeaderText="Test Scores" />
            <asp:BoundField DataField="skills_score" HeaderText="Skills Score" />
            <asp:BoundField DataField="Name" HeaderText="Applicant Name" />
            <asp:BoundField DataField="email" HeaderText="Applicant Email ID" />
            <asp:BoundField DataField="Application_Status" HeaderText="Application Status" />
            <asp:TemplateField HeaderText="Resume">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("CV_File", "{0}") %>' Target="_blank" ForeColor="#3366FF">Download Resume</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="education" HeaderText="Education " />
            <asp:BoundField DataField="experience" HeaderText="Experience" />
            <asp:TemplateField HeaderText="Accept">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/icons/accept.png" Width="65px" CommandArgument='<% #Eval("Applicationid", "{0}") %>' OnClick="ImageButton1_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Reject">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/icons/reject.png" Width="65px" CommandArgument='<% #Eval("Applicationid", "{0}") %>' OnClick="ImageButton2_Click"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>
