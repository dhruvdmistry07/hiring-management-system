﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace IDP_Project_01.hr
{
    public partial class hr_profile : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["hrid"] == null)
                    Response.Redirect("../hr_login.aspx");
                string hrid = Session["hrid"].ToString();
                string q = "select * from hrtbl where hrid='" + hrid + "'";
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                cmd.ExecuteNonQuery();
                da.Fill(ds, "hrtbl");
                txtname.Text = ds.Tables["hrtbl"].Rows[0][1].ToString();
                txtemail.Text = ds.Tables["hrtbl"].Rows[0][3].ToString();
                txtcontact.Text = ds.Tables["hrtbl"].Rows[0][2].ToString();



            }
        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            string hrid = Session["aid"].ToString();
            string q = "update hrtbl set hrname='" + txtname.Text + "',hremail='" + txtemail.Text + "',hrcontact='" + txtcontact.Text + "' where hrid='" + hrid + "'";

            if (con.State == ConnectionState.Closed)
                con.Open();
            cmd = new SqlCommand(q, con);


            int i = cmd.ExecuteNonQuery();
            if (i > 0)
            {
                Response.Write("<script>alert('Profile updated sucessfully!'); window.location = 'Dashboard_hr.aspx';</script>");
            }
            else
            {
                Response.Write("<script>alert ('Profile updated failed!'); window.location = 'hr_profile.aspx';</script>");
            }
        }
    }
}