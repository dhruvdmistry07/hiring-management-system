﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="postquestions.aspx.cs" Inherits="IDP_Project_01.hr.postquestions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Post Question</h2>
    <br />
    <asp:Label ID="lblquest" runat="server" Text="Question:"></asp:Label>
    <br />
    <asp:TextBox ID="txtquest" runat="server" TextMode="MultiLine"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtquest" ErrorMessage="  *Question is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lblaopt" runat="server" Text="Option A:"></asp:Label>
&nbsp;&nbsp;
    <asp:TextBox ID="txta" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txta" ErrorMessage="  *Minimum two options are required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lblbopt" runat="server" Text="Option B:"></asp:Label>
&nbsp;&nbsp;
    <asp:TextBox ID="txtb" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtb" ErrorMessage="  *Minimum two options are required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <asp:Label ID="lbloptc" runat="server" Text="Option C:"></asp:Label>
&nbsp;&nbsp;
    <asp:TextBox ID="txtc" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lbloptd" runat="server" Text="Option D:"></asp:Label>
&nbsp;&nbsp;
    <asp:TextBox ID="txtd" runat="server"></asp:TextBox>
    <br />
    <br />
    <br />
    <asp:Label ID="lblans" runat="server" Text="Correct Answer:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList2" runat="server">
        <asp:ListItem>A</asp:ListItem>
        <asp:ListItem>B</asp:ListItem>
        <asp:ListItem>C</asp:ListItem>
        <asp:ListItem>D</asp:ListItem>
    </asp:DropDownList>
    <br />
    <br />
    <br />
    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Submit" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" CausesValidation="False" Text="Reset" OnClick="btnreset_Click" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
