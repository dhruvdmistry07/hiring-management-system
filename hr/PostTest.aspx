﻿<%@ Page Title="" Language="C#" MasterPageFile="~/hr/hrmaster.Master" AutoEventWireup="true" CodeBehind="PostTest.aspx.cs" Inherits="IDP_Project_01.hr.PostTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Post Test</h2><br />
    <asp:Label ID="lbljobid" runat="server" Text="Job ID:"></asp:Label>
    <br />
    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="jobtitle" DataValueField="jobid">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Cv_DBConnectionString %>" SelectCommand="SELECT * FROM [Jobtbl]"></asp:SqlDataSource>
    <br />
    <br />
    <br />
    <asp:Label ID="lbltitle" runat="server" Text="Test Title:"></asp:Label>
    <br />
    <asp:TextBox ID="txttitle" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttitle" ErrorMessage="  * Test title is required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Label ID="lbltot" runat="server" Text="Total Questions:"></asp:Label>
    <br />
    <asp:TextBox ID="txttot" runat="server" TextMode="Number"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttot" ErrorMessage="  *Total questions are required!" ForeColor="Red"></asp:RequiredFieldValidator>
    <br />
    <br />
    <br />
    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Submit" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnreset" runat="server" Text="Reset" OnClick="btnreset_Click" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
