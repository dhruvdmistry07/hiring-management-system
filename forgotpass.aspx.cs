﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

namespace IDP_Project_01
{
    public partial class admin_forgotpass : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Cv_DBConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void sendemail(string rec_email, string pwd)
        {

            // write code to sent an email 


            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress("mail");
            mail.To.Add(rec_email);
            mail.Subject = "Password recovery email ";
            mail.Body = "Your Login password is " + pwd;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("mail", "pwd");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            Response.Write("<script> alert('Password sent on your registred email'); window.location = 'index.html'; </script>");
        }

        protected void btnrecover_Click(object sender, EventArgs e)
        {
            string pwd = "";
            if (con.State == ConnectionState.Closed)
                con.Open();

            if (DropDownList1.SelectedItem.ToString() == "Admin")
            {
                string q = "select * from admintbl where aemail = '" + txtemail.Text + "'";
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();

                cmd.ExecuteNonQuery();
                da.Fill(ds, "admintbl");
                if (ds.Tables["admintbl"].Rows.Count > 0)
                {
                    pwd = ds.Tables["admintbl"].Rows[0][3].ToString();

                    sendemail(txtemail.Text, pwd);

                }
                else
                {
                    Response.Write("<script>alert('Admin data not found !!'); window.location ='ForgetPassword.aspx'; </script>");

                }

            }

            else if (DropDownList1.SelectedItem.ToString() == "HR")
            {
                string q = "select * from hrtbl where hremail = '" + txtemail.Text + "'";
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();

                cmd.ExecuteNonQuery();
                da.Fill(ds, "hrtbl");
                if (ds.Tables["hrtbl"].Rows.Count > 0)
                {
                    pwd = ds.Tables["hrtbl"].Rows[0][4].ToString();

                    sendemail(txtemail.Text, pwd);

                }
                else
                {
                    Response.Write("<script>alert('HR data not found !!'); window.location ='ForgetPassword.aspx'; </script>");

                }


            }

            else if (DropDownList1.SelectedItem.ToString() == "User")
            {
                string q = "select * from Applicant where appemail = '" + txtemail.Text + "'";
                cmd = new SqlCommand(q, con);
                da = new SqlDataAdapter(cmd);
                ds = new DataSet();

                cmd.ExecuteNonQuery();
                da.Fill(ds, "appemail");
                if (ds.Tables["appemail"].Rows.Count > 0)
                {
                    pwd = ds.Tables["appemail"].Rows[0][4].ToString();

                    sendemail(txtemail.Text, pwd);

                }
                else
                {
                    Response.Write("<script>alert('Applicant data not found !!'); window.location ='ForgetPassword.aspx'; </script>");

                }



            }

        }
    }
}